package com.example.steva.climate_app;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.steva.climate_app.model.City;
import com.example.steva.climate_app.model.ClimateResponse;
import com.example.steva.climate_app.model.ListTemperature;
import com.example.steva.climate_app.networking.ClimateAPI;

import org.w3c.dom.Text;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {
    private City city;
    private String cityName = "Jakarta";
    private ListView listViewTemperature;
    private TextView labelCity;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Setup Spinner Pilih Kota
        Spinner spinner = (Spinner) findViewById(R.id.spinnerPilihKota);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.city_arrays, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                cityName = (String) parent.getItemAtPosition(position);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        //Inisialisasi list view dan label city
        listViewTemperature = (ListView) findViewById(R.id.listViewTemperature);
        labelCity = (TextView) findViewById(R.id.labelCity);


    }


    //Temperature Submit Button Function
    public void getCityDailyTemperature(View view){

        Call<ClimateResponse> call  = ClimateAPI.getApi().getClimates(cityName,"json","metric","5");
        call.enqueue(new Callback<ClimateResponse>() {
            @Override
            public void onResponse(Call<ClimateResponse> call, Response<ClimateResponse> response) {
                ClimateResponse getClimateResponse = response.body();
                city = getClimateResponse.getCity();
                TemperatureAdapter temperatureAdapter = new TemperatureAdapter(getClimateResponse.getListTemperature());
                listViewTemperature.setAdapter(temperatureAdapter);
                labelCity.setText(cityName);
            }

            @Override
            public void onFailure(Call<ClimateResponse> call, Throwable t) {
                Toast.makeText(MainActivity.this, "Error Loading Data", Toast.LENGTH_SHORT).show();
            }
        });
    }

    //Adapter for ListTemperature
    public class TemperatureAdapter extends BaseAdapter {
        private List<ListTemperature> listTemperatures;

        public List<ListTemperature> getListTemperatures() {
            return listTemperatures;
        }

        public void setListTemperatures(List<ListTemperature> listTemperatures) {
            this.listTemperatures = listTemperatures;
        }

        public TemperatureAdapter(List<ListTemperature> listTemperatures){
            this.listTemperatures = listTemperatures;
        }
        @Override
        public int getCount() {
            return listTemperatures.size();
        }

        @Override
        public Object getItem(int position) {
            return listTemperatures.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View view, ViewGroup viewGroup) {
            if(view==null)
            {
                LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                view = inflater.inflate(R.layout.list_daily_temperature,viewGroup,false);
            }
            ListTemperature listTemperature = listTemperatures.get(position);
            TextView txtTemperatureDate = (TextView)view.findViewById(R.id.txtTemperatureDate);
            TextView txtTemperatureDegree = (TextView)view.findViewById(R.id.txtTemperatureDegree);
            TextView txtTemperatureVariance = (TextView)view.findViewById(R.id.txtTemperatureVariance);

            txtTemperatureDate.setText("Date: " + listTemperature.getDtInDate());
            txtTemperatureDegree.setText("Temperature: " + listTemperature.getTemperature().getDay().toString() + "C");
            txtTemperatureVariance.setText("Variance: " + listTemperature.getTemperature().getVariance() + "C");

            return view;

        }

    }
}
