package com.example.steva.climate_app.networking;

import com.example.steva.climate_app.model.ClimateResponse;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by steva on 2/22/2017.
 */

public class ClimateAPI {
    private static final String APIKEY = "481e3bc28e5264e5607c2b65b449bfc1";
    private static final String APIPATH = "http://api.openweathermap.org/data/2.5/forecast/";
    private static ClimateService climateService = null;

    public static ClimateService getApi(){
        if(climateService==null){
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(APIPATH)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            climateService = retrofit.create(ClimateService.class);
        }
        return climateService;
    }


    public interface ClimateService{
        @GET("daily?APPID=" + APIKEY)
        Call<ClimateResponse> getClimates(@Query("q") String city, @Query("mode") String mode, @Query("units") String units,@Query("cnt") String cnt);
    }
}
