package com.example.steva.climate_app.utils;

import android.util.Log;

import java.sql.Date;
import java.text.SimpleDateFormat;

/**
 * Created by steva on 3/1/2017.
 */

public class DateUtils {
    public static String convertTimeStampToDate(Long time){
        String date = "";
        if(time!=null) {
            Date inputDate = new Date(time * 1000);
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("YYYY-MM-dd");
            date = simpleDateFormat.format(inputDate);
        }
        return date;
    }
}
